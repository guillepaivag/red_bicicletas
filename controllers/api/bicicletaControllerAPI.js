const Bicicleta = require("../../models/Bicicleta");

const constrollerBicicletasAPI = {}

constrollerBicicletasAPI.bicicleta_list = async (req, res) => {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    })
}

constrollerBicicletasAPI.bicicleta_create = async (req, res) => {
    const bici = new Bicicleta({
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lon],
    })

    Bicicleta.add( bici )

    res.status(200).json({
        bicicleta: bici
    })
}

constrollerBicicletasAPI.bicicleta_update = (req, res) => {
    const uid = req.params.uid
    const { color, modelo, lat, lon } = req.body

    let bici = Bicicleta.findByUID( uid )

    bici.color = color
    bici.modelo = modelo
    bici.ubicacion = [ lat, lon ]

    res.status(200).json({
        bicicleta: bici
    })
}

constrollerBicicletasAPI.bicicleta_delete = async (req, res) => {
    console.log('req.params.uid', req.params.uid)
    
    Bicicleta.removeByUID( req.params.uid )

    res.status(204).send()
}

module.exports = constrollerBicicletasAPI