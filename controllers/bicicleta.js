const Bicicleta = require("../models/Bicicleta");

const constrollerBicicletas = {}

constrollerBicicletas.bicicleta_list = async (req, res) => {
    res.render('bicicletas/index', { bicis: Bicicleta.allBicis })
}

constrollerBicicletas.bicicleta_create_get = async (req, res) => {
    res.render('bicicletas/create')
}

constrollerBicicletas.bicicleta_create_post = async (req, res) => {
    const bici = new Bicicleta({
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lon],
    })

    Bicicleta.add( bici )

    res.redirect('/bicicletas')
}

constrollerBicicletas.bicicleta_update_get = (req, res) => {
    const uid = req.params.uid
    const bici = Bicicleta.findByUID( uid )

    res.render(`bicicletas/update`, { bici })
}

constrollerBicicletas.bicicleta_update_post = (req, res) => {
    const uid = req.params.uid
    const { color, modelo, lat, lon } = req.body

    let bici = Bicicleta.findByUID( uid )

    bici.color = color
    bici.modelo = modelo
    bici.ubicacion = [ lat, lon ]

    res.redirect('/bicicletas')
}

constrollerBicicletas.bicicleta_delete_post = async (req, res) => {
    console.log('req.params.uid', req.params.uid)
    
    Bicicleta.removeByUID( req.params.uid )

    res.redirect('/bicicletas')
}

module.exports = constrollerBicicletas