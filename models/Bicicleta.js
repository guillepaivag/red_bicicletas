const { v4: uuidv4 } = require('uuid');

class Bicicleta {
    constructor (data = {}) {
        const { uid, color, modelo, ubicacion } = data

        this.uid = uid ? uid : uuidv4()
        this.color = color ? color : 'azul'
        this.modelo = modelo ? modelo : 'urbano'
        this.ubicacion = ubicacion ? ubicacion : [0, 0]
    }

    // Metodos

    toString () {
        return `UID: ${this.uid} | Modelo: ${this.modelo}`
    }





    // Static

    static allBicis = []
    static add = function ( bici = new Bicicleta() ) {
        Bicicleta.allBicis.push( bici )
    }

    static findByUID = function ( uid = '' ) {
        const bici = Bicicleta.allBicis.find( v => v.uid === uid )

        if (bici) return bici
        else throw new Error (`No se encontro la bicicleta con la uid ${uid}`)
    }

    static removeByUID = function ( uid ) {
        Bicicleta.findByUID( uid )

        for (let i = 0; i < Bicicleta.allBicis.length; i++) {
            const biciAux = Bicicleta.allBicis[i]
            
            if (biciAux.uid === uid) {
                Bicicleta.allBicis.splice(i, 1)
                break
            }
        }
    }
}

module.exports = Bicicleta