let map = L.map('main_map').setView([-25.341736017278894, -57.54833801988255], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

$.ajax({
    dataType: 'json',
    url: 'api/bicicletas',
    success: function ( result ) {
        console.log('result', result)
        
        result.bicicletas.forEach(bici => {
            console.log('bici', bici)
            L.marker(bici.ubicacion, { title: bici.uid }).addTo(map)
            .bindPopup(`${bici.modelo} - ${bici.color}`)
            .openPopup();
        });
    }
})