const { Router } = require('express')
const bicicletaControllerAPI = require('../../controllers/api/bicicletaControllerAPI')
const router = Router()

router.get('/', bicicletaControllerAPI.bicicleta_list)
router.post('/create', bicicletaControllerAPI.bicicleta_create)
router.put('/update/:uid', bicicletaControllerAPI.bicicleta_update)
router.delete('/delete/:uid', bicicletaControllerAPI.bicicleta_delete)

module.exports = router