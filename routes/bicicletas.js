const { Router } = require('express')
const constrollerBicicletas = require('../controllers/bicicleta')
const router = Router()

router.get('/', constrollerBicicletas.bicicleta_list)

router.get('/create', constrollerBicicletas.bicicleta_create_get)
router.post('/create', constrollerBicicletas.bicicleta_create_post)

router.get('/update/:uid', constrollerBicicletas.bicicleta_update_get)
router.post('/update/:uid', constrollerBicicletas.bicicleta_update_post)

router.post('/delete/:uid', constrollerBicicletas.bicicleta_delete_post)

module.exports = router